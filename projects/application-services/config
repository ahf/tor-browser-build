# vim: filetype=yaml sw=2
filename: '[% project %]-[% c("version") %]-[% c("var/build_id") %].tar.gz'
version: 74.0.1
git_hash: ad7b64fa03eeeb00815125e635d1fb8809befd40
git_url: https://github.com/mozilla/application-services
git_submodule: 1

var:
  container:
    use_container: 1
  # This should be updated when the list of gradle dependencies is changed.
  gradle_dependencies_version: 5
  # This should be updated when the list of rust dependencies is changed.
  rust_vendor_version: 5
  gradle_version: 6.5

input_files:
  - project: container-image
    pkg_type: build
  - project: '[% c("var/compiler") %]'
    name: '[% c("var/compiler") %]'
    pkg_type: build
  - project: gradle
    name: gradle
    pkg_type: build
  - project: rust
    name: rust
    pkg_type: build
  - project: uniffi-rs
    name: uniffi-rs
    pkg_type: build
  - project: nss
    name: nss-armv7
    pkg_type: build
    target_prepend:
      - android-armv7
  - project: nss
    name: nss-aarch64
    pkg_type: build
    target_prepend:
      - android-aarch64
  - project: nss
    name: nss-x86
    pkg_type: build
    target_prepend:
      - android-x86
  - project: nss
    name: nss-x86_64
    pkg_type: build
    target_prepend:
      - android-x86_64
  - project: sqlcipher
    name: sqlcipher-armv7
    pkg_type: build
    target_prepend:
      - android-armv7
  - project: sqlcipher
    name: sqlcipher-aarch64
    pkg_type: build
    target_prepend:
      - android-aarch64
  - project: sqlcipher
    name: sqlcipher-x86
    pkg_type: build
    target_prepend:
      - android-x86
  - project: sqlcipher
    name: sqlcipher-x86_64
    pkg_type: build
    target_prepend:
      - android-x86_64
  - name: python
    project: python
    enable: '[% !c("var/fetch_gradle_dependencies") %]'
  - filename: 'gradle-dependencies-[% c("var/gradle_dependencies_version") %]'
    name: gradle-dependencies
    exec: '[% INCLUDE "fetch-gradle-dependencies" %]'
    enable: '[% !c("var/fetch_gradle_dependencies") %]'
  - URL: https://people.torproject.org/~gk/mirrors/sources/glean-parser-[% c('var/glean_parser') %].tar.bz2
    sha256sum: 19dbdd4958022a1a638e0217489ab722fe7d4f588f1978a4ae162f93e75694c2
    enable: '[% !c("var/fetch_gradle_dependencies") %]'
  # Use `make cargo_vendor-application-services` to re-generate the vendor tarball
  - URL: https://people.torproject.org/~boklm/mirrors/sources/application-services-vendor-[% c('var/rust_vendor_version') %].tar.bz2
    sha256sum: 5f1c9e1bc0db283e88516bd1d78187145bacbf4aeb72972ac09875c1147d2215
  - filename: no-git.patch
  - filename: mavenLocal.patch
    enable: '[% !c("var/fetch_gradle_dependencies") %]'
  - filename: target.patch
  - filename: viaduct-workaround.patch
  - filename: update-cargo-lock.patch
  - filename: gen_gradle_deps_file.sh
    enable: '[% c("var/fetch_gradle_dependencies") %]'
  - filename: 0001-Store-the-universe-of-known-types-in-an-ordered-BTre.patch

steps:
  list_toolchain_updates:
    #git_hash: 'v74.0.1'
    input_files: []
    var:
      container:
        use_container: 0

  get_gradle_dependencies_list:
    filename: 'gradle-dependencies-list-[% c("version") %].txt'
    get_gradle_dependencies_list: '[% INCLUDE build %]'
    var:
      fetch_gradle_dependencies: 1

  cargo_vendor:
    filename: '[% project %]-vendor-[% c("version") %].tar.bz2'
    var:
      cargo_vendor_opts: '-s components/external/nimbus-sdk/nimbus/Cargo.toml'
      pre_cargo_vendor: |
        patch -p1 < $rootdir/update-cargo-lock.patch
        patch -d components/external/nimbus-sdk -p1 < $rootdir/viaduct-workaround.patch
    input_files:
      - project: container-image
        pkg_type: build
      - filename: viaduct-workaround.patch
      - filename: update-cargo-lock.patch
